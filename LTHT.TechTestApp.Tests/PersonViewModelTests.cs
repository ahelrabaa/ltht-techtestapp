﻿namespace LTHT.TechTestApp.Tests
{
    using System;

    using LTHT.TechTestApp.Interfaces.ViewModels;
    using LTHT.TechTestApp.Models;
    using LTHT.TechTestApp.ViewModels;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class PersonViewModelTests
    {
        [TestMethod]
        public void FirstNameInitial_For_Person_With_FirstName_Bob_Returns_B()
        {
            var person = new Person { FirstName = "Bob", LastName = "Green" };

            var personViewModel = GetPersonViewModel(person);
            const string Expected = "B";

            var actual = personViewModel.FirstNameInitial;

            Assert.AreEqual(Expected, actual);
        }

        [TestMethod]
        public void FullName_Returns_FirstName_and_Surname_Together()
        {
            var person = new Person { FirstName = "first", LastName = "last" };

            var personViewModel = GetPersonViewModel(person);

            const string Expected = "first last";

            var actual = personViewModel.FullName;

            Assert.AreEqual(Expected, actual);
        }

        [TestMethod]
        public void HasPalindromeFullName_For_FirstName_Bob_And_LastName_Bob_Is_True_()
        {
            var person = new Person { FirstName = "Bob", LastName = "Bob" };

            var personViewModel = GetPersonViewModel(person);

            Assert.IsTrue(personViewModel.HasPalindromeFullName);
        }

        [TestMethod]
        public void HasPalindromeFullName_For_FirstName_Mo_And_LastName_Green_Is_False_()
        {
            var person = new Person { FirstName = "Mo", LastName = "Green" };

            var personViewModel = GetPersonViewModel(person);

            Assert.IsFalse(personViewModel.HasPalindromeFullName);
        }

        [ExpectedException(typeof(ArgumentNullException))]
        [TestMethod]
        public void PersonViewModel_Has_To_Be_Initialized_With_A_Person()
        {
            GetPersonViewModel(null);
        }

        private static IPersonViewModel GetPersonViewModel(Person person)
        {
            return new PersonViewModel(person);
        }
    }
}