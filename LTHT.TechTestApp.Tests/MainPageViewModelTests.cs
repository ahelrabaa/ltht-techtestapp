﻿namespace LTHT.TechTestApp.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using LTHT.TechTestApp.Interfaces.Models;
    using LTHT.TechTestApp.Interfaces.ViewModels;
    using LTHT.TechTestApp.Models;
    using LTHT.TechTestApp.ViewModels;
    using LTHT.TechTestApp.Views;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class MainPageViewModelTests
    {
        [TestMethod]
        public async Task FilterPeopleCommand_4_People_In_List_Filter_With_bb_returns_2_In_List()
        {
            IPeopleService fakePeopleService =
                new FakePeopleService(
                    () =>
                        new List<Person>
                            {
                                new Person { FirstName = "aa1", },
                                new Person { FirstName = "aa2", },
                                new Person { FirstName = "bb1", },
                                new Person { FirstName = "bb2", },
                            });
            IMainPageViewModel mainPageViewModel = new MainPageViewModel(fakePeopleService, new NavigationService(), new DialogService());
            await mainPageViewModel.Initialization;
            mainPageViewModel.FilterPeopleCommand.Execute("bb");

            const int Expected = 2;
            var personViewModels = mainPageViewModel.People.SelectMany(models => models).ToList();
            var actual = personViewModels.Count;

            Assert.AreEqual(Expected, actual);
        }

        [TestMethod]
        public async Task
            Initializing_IPeopleService_Returns_4_People_2_Have_Names_Starting_With_A_And_2_Have_Names_Starting_With_B_Therefore_People_Groups_Should_Be_2
            ()
        {
            IPeopleService fakePeopleService =
                new FakePeopleService(
                    () =>
                        new List<Person>
                            {
                                new Person { FirstName = "aa1", },
                                new Person { FirstName = "aa2", },
                                new Person { FirstName = "bb1", },
                                new Person { FirstName = "bb2", },
                            });
            IMainPageViewModel mainPageViewModel = new MainPageViewModel(fakePeopleService, new NavigationService(), new DialogService());
            await mainPageViewModel.Initialization;
            const int Expected = 2;

            var actual = mainPageViewModel.People.Count;

            Assert.AreEqual(Expected, actual);
        }
    }

    public class FakePeopleService : IPeopleService
    {
        private readonly Func<IEnumerable<Person>> peopleFactory;

        public FakePeopleService(Func<IEnumerable<Person>> peopleFactory)
        {
            this.peopleFactory = peopleFactory;
        }

        public Task<IEnumerable<Person>> GetPeopleAsync()
        {
            var people = this.peopleFactory();

            return Task.FromResult(people);
        }

        public Task<Person> DeletePersonAsync(Person person)
        {
            throw new NotImplementedException();
        }

        public Task<Person> SavePersonAsync(Person person)
        {
            throw new NotImplementedException();
        }
    }
}