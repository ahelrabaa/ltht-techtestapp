﻿namespace LTHT.TechTestApp.ViewModels
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Input;

    using LTHT.TechTestApp.Interfaces.Models;
    using LTHT.TechTestApp.Interfaces.ViewModels;
    using LTHT.TechTestApp.Interfaces.Views;
    using LTHT.TechTestApp.Models;

    using Xamarin.Forms;

    public class PersonDetailPageViewModel : ViewModelBase, IPersonDetailPageViewModel
    {
        private readonly IDialogService dialogService;

        private readonly INavigationService navigationService;

        private readonly IPeopleService peopleService;

        private Command deleteCommand;

        private Command goBackCommand;

        private Command saveCommand;

        private string title;

        public PersonDetailPageViewModel(
            IPersonViewModel personViewModel,
            IColoursViewModel coloursViewModel,
            INavigationService navigationService,
            IPeopleService peopleService,
            IDialogService dialogService)
        {
            this.navigationService = navigationService;
            this.peopleService = peopleService;
            this.dialogService = dialogService;
            this.ColoursViewModel = coloursViewModel;
            this.InitializePerson(personViewModel);
            this.Initialization = this.InitializeColoursAsync();
        }

        public IColoursViewModel ColoursViewModel { get; }

        public ICommand DeleteCommand
            =>
                this.deleteCommand
                ?? (this.deleteCommand = new Command(async () => await this.DeletePersonAsync(), () => !this.IsBusy));

        public ICommand GoBackCommand
            =>
                this.goBackCommand
                ?? (this.goBackCommand = new Command(async () => await this.navigationService.GoBackAsync()));

        public Task Initialization { get; }

        public bool IsDeleteEnabled { get; private set; }

        public IPersonViewModel PersonViewModel { get; private set; }

        public ICommand SaveCommand
            =>
                this.saveCommand
                ?? (this.saveCommand = new Command(async () => await this.SaveDataAsync(), () => !this.IsBusy));

        public string Title
        {
            get
            {
                return this.title;
            }

            set
            {
                this.title = value;
                this.OnPropertyChanged();
            }
        }

        private static bool ColourMatch(IColourViewModel model, IColourViewModel colourViewModel)
        {
            return model.Colour.Id == colourViewModel.Colour.Id;
        }

        private async Task DeletePersonAsync()
        {
            try
            {
                this.IsBusy = true;
                await this.peopleService.DeletePersonAsync(this.PersonViewModel.Person);
                this.IsBusy = false;
                await this.navigationService.GoBackAsync();
            }
            catch (Exception exception)
            {
                await this.dialogService.DisplayMessageAsync("Oops!", exception.Message);
            }
            finally
            {
                this.IsBusy = false;
            }
        }

        private async Task InitializeColoursAsync()
        {
            await this.ColoursViewModel.Initialization;
            foreach (var colourViewModel in this.PersonViewModel.SelectedColours)
            {
                this.ColoursViewModel.Colours.Single(model => ColourMatch(model, colourViewModel)).IsSelected = true;
            }
        }

        private void InitializePerson(IPersonViewModel personViewModel)
        {
            if (personViewModel == null)
            {
                this.SetupToAddPerson();
                return;
            }

            this.SetupToEditPerson(personViewModel);
        }

        private async Task SaveDataAsync()
        {
            this.UpdateSelectedColours();

            try
            {
                var person = this.PersonViewModel.Person;

                if (string.IsNullOrWhiteSpace(person.FirstName))
                {
                    await this.dialogService.DisplayMessageAsync("Required", "First name required");
                    return;
                }

                if (string.IsNullOrWhiteSpace(person.LastName))
                {
                    await this.dialogService.DisplayMessageAsync("Required", "Last name required");
                    return;
                }

                this.IsBusy = true;
                await this.peopleService.SavePersonAsync(this.PersonViewModel.Person);
                this.IsBusy = false;
                await this.navigationService.GoBackAsync();
            }
            catch (Exception exception)
            {
                await this.dialogService.DisplayMessageAsync("Oops!", exception.Message);
            }
            finally
            {
                this.IsBusy = false;
            }
        }

        private void SetupToAddPerson()
        {
            this.PersonViewModel = new PersonViewModel(new Person());
            this.Title = "Add Person";
        }

        private void SetupToEditPerson(IPersonViewModel personViewModel)
        {
            this.PersonViewModel = personViewModel;
            this.Title = $"Edit {this.PersonViewModel.FullName}";
            this.IsDeleteEnabled = true;
        }

        private void UpdateSelectedColours()
        {
            this.PersonViewModel.UpdateSelectedColours(this.ColoursViewModel.SelectedColours);
        }
    }
}