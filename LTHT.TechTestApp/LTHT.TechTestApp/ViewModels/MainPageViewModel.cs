﻿namespace LTHT.TechTestApp.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Input;

    using LTHT.TechTestApp.Helpers;
    using LTHT.TechTestApp.Interfaces.Models;
    using LTHT.TechTestApp.Interfaces.ViewModels;
    using LTHT.TechTestApp.Interfaces.Views;

    using Xamarin.Forms;

    public class MainPageViewModel : ViewModelBase, IMainPageViewModel
    {
        private readonly IDialogService dialogService;

        private readonly INavigationService navigationService;

        private readonly IPeopleService peopleService;

        private List<PersonViewModel> allPeople;

        private bool isRefreshing;

        private ICommand refreshPeopleCommand;

        private ICommand viewPersonDetailCommand;

        public MainPageViewModel(
            IPeopleService peopleService,
            INavigationService navigationService,
            IDialogService dialogService)
        {
            this.peopleService = peopleService;
            this.navigationService = navigationService;
            this.dialogService = dialogService;
            this.People = new ObservableCollection<Grouping<string, IPersonViewModel>>();
            this.FilterPeopleCommand = new Command<string>(this.FilterPeople);
        }

        public ICommand FilterPeopleCommand { get; }

        public Task Initialization => this.InitializeAsync();

        public bool IsRefreshing
        {
            get
            {
                return this.isRefreshing;
            }

            private set
            {
                this.isRefreshing = value;
                this.OnPropertyChanged();
            }
        }

        public IList<Grouping<string, IPersonViewModel>> People { get; }

        public ICommand RefreshPeopleCommand
            =>
                this.refreshPeopleCommand
                ?? (this.refreshPeopleCommand = new Command(async () => await this.RefreshPeopleAsync()));

        public ICommand ViewPersonDetailCommand
            =>
                this.viewPersonDetailCommand
                ?? (this.viewPersonDetailCommand =
                        new Command<IPersonViewModel>(async model => await this.NavigateToPersonDetailAsync(model)));

        private void FilterPeople(string filter)
        {
            var filteredPeople = this.allPeople.Where(model => model.FullName.ToLowerInvariant().Contains(filter));
            this.GroupPeople(filteredPeople);
        }

        private async Task GetPeopleAsync()
        {
            var people = await this.peopleService.GetPeopleAsync();
            this.allPeople =
                people.OrderBy(person => person.FirstName).Select(person => new PersonViewModel(person)).ToList();
            this.GroupPeople(this.allPeople);
        }

        private void GroupPeople(IEnumerable<PersonViewModel> people)
        {
            this.People.Clear();
            var groupedByFirstLetterOfFirstName = people.GroupBy(model => model.FirstNameInitial.ToUpperInvariant());

            foreach (var group in groupedByFirstLetterOfFirstName)
            {
                this.People.Add(new Grouping<string, IPersonViewModel>(group.Key, group));
            }
        }

        private async Task InitializeAsync()
        {
            try
            {
                this.IsBusy = true;
                await this.GetPeopleAsync();
            }
            catch (Exception exception)
            {
                await this.dialogService.DisplayMessageAsync("Oops!", exception.Message);
            }
            finally
            {
                this.IsBusy = false;
            }
        }

        private Task NavigateToPersonDetailAsync(IPersonViewModel personViewModel)
        {
            return this.navigationService.NavigateToPersonDetialAsync(personViewModel);
        }

        private async Task RefreshPeopleAsync()
        {
            try
            {
                this.IsRefreshing = true;
                await this.GetPeopleAsync();
            }
            catch (Exception exception)
            {
                await this.dialogService.DisplayMessageAsync("Oops!", exception.Message);
            }
            finally
            {
                this.IsRefreshing = false;
            }
        }
    }
}