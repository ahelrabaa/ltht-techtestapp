namespace LTHT.TechTestApp.ViewModels
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;

    using LTHT.TechTestApp.Interfaces.Models;
    using LTHT.TechTestApp.Interfaces.ViewModels;

    public class ColoursViewModel : ViewModelBase, IColoursViewModel
    {
        private readonly IColoursService coloursService;

        public ColoursViewModel(IColoursService coloursService)
        {
            this.coloursService = coloursService;
            this.Colours = new ObservableCollection<IColourViewModel>();
            this.Initialization = this.GetColoursAsync();
        }

        public IList<IColourViewModel> Colours { get; }

        public Task Initialization { get; }

        public IList<IColourViewModel> SelectedColours => this.Colours.Where(model => model.IsSelected).ToList();

        private async Task GetColoursAsync()
        {
            var colours = await this.coloursService.GetColoursAsync();

            foreach (var colour in colours)
            {
                this.Colours.Add(new ColourViewModel(colour));
            }
        }
    }
}