﻿namespace LTHT.TechTestApp.ViewModels
{
    using LTHT.TechTestApp.Helpers;
    using LTHT.TechTestApp.Interfaces.ViewModels;

    public class ViewModelBase : ObservableObject, IViewModel
    {
        private bool isBusy;

        public bool IsBusy
        {
            get
            {
                return this.isBusy;
            }

            set
            {
                this.isBusy = value;
                this.OnPropertyChanged();
            }
        }
    }
}