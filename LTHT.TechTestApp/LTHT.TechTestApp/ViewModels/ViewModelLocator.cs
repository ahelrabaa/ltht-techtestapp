namespace LTHT.TechTestApp.ViewModels
{
    using LTHT.TechTestApp.Interfaces.Models;
    using LTHT.TechTestApp.Interfaces.ViewModels;
    using LTHT.TechTestApp.Models;
    using LTHT.TechTestApp.Views;

    public class ViewModelLocator
    {
        private readonly RestClient restClient;

        public ViewModelLocator()
        {
            this.restClient = new RestClient();
        }

        public IColoursViewModel ColoursViewModel => new ColoursViewModel(new ColoursService(this.restClient));

        public IMainPageViewModel MainPageViewModel
            => new MainPageViewModel(new PeopleService(this.restClient), new NavigationService(), new DialogService());

        public IPersonDetailPageViewModel PersonDetailPageViewModel(IPersonViewModel personViewModel)
            =>
                new PersonDetailPageViewModel(
                    personViewModel,
                    this.ColoursViewModel,
                    new NavigationService(),
                    new PeopleService(this.restClient),
                    new DialogService());
    }
}