namespace LTHT.TechTestApp.ViewModels
{
    using LTHT.TechTestApp.Interfaces.ViewModels;
    using LTHT.TechTestApp.Models;

    public class ColourViewModel : ViewModelBase, IColourViewModel
    {
        private bool isSelected;

        public ColourViewModel(Colour colour)
        {
            this.Colour = colour;
        }

        public Colour Colour { get; }

        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }

            set
            {
                this.isSelected = value;
                this.OnPropertyChanged();
            }
        }
    }
}