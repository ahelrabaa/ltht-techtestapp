﻿namespace LTHT.TechTestApp.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    using LTHT.TechTestApp.Interfaces.ViewModels;
    using LTHT.TechTestApp.Models;

    public class PersonViewModel : ViewModelBase, IPersonViewModel
    {
        private bool hasPalindromeFullName;

        public PersonViewModel(Person person)
        {
            if (person == null)
            {
                throw new ArgumentNullException(nameof(person));
            }

            this.Person = person;
            this.SelectedColours = new ObservableCollection<IColourViewModel>();

            var personFirstName = this.Person.FirstName;
            if (personFirstName == null)
            {
                return;
            }

            this.FirstNameInitial = personFirstName[0].ToString();
            this.CheckIfFullNameIsAPalindrome();

            var personColours = this.Person.Colours;
            if (personColours != null)
            {
                foreach (var colour in personColours)
                {
                    this.SelectedColours.Add(new ColourViewModel(colour) { IsSelected = true });
                }
            }
        }

        public string FirstName
        {
            get
            {
                return this.Person.FirstName;
            }

            set
            {
                this.Person.FirstName = value;
                this.OnPropertyChanged();
                this.UpdateFullNameAndCheckForPalindrome();
            }
        }

        public string FirstNameInitial { get; }

        public string FullName => $"{this.FirstName} {this.LastName}";

        public bool HasPalindromeFullName
        {
            get
            {
                return this.hasPalindromeFullName;
            }

            private set
            {
                this.hasPalindromeFullName = value;
                this.OnPropertyChanged();
            }
        }

        public string LastName
        {
            get
            {
                return this.Person.LastName;
            }

            set
            {
                this.Person.LastName = value;
                this.OnPropertyChanged();
                this.UpdateFullNameAndCheckForPalindrome();
            }
        }

        public Person Person { get; }

        public IList<IColourViewModel> SelectedColours { get; }

        public void UpdateSelectedColours(IList<IColourViewModel> selectedColours)
        {
            this.SelectedColours.Clear();

            foreach (var colourViewModel in selectedColours)
            {
                this.SelectedColours.Add(colourViewModel);
            }

            this.Person.Colours = selectedColours.Select(model => model.Colour).ToList();
        }

        private void CheckIfFullNameIsAPalindrome()
        {
            var forward = this.GetFullNameWithoutSpaces().ToLowerInvariant();
            var forwardCharArray = forward.ToCharArray();
            var reversedCharArray = forwardCharArray.Reverse();
            var isPalindrome = forwardCharArray.SequenceEqual(reversedCharArray);

            this.HasPalindromeFullName = isPalindrome;
        }

        private string GetFullNameWithoutSpaces()
        {
            return this.FullName.Replace(" ", string.Empty);
        }

        private void UpdateFullNameAndCheckForPalindrome()
        {
            this.OnPropertyChanged(nameof(this.FullName));
            this.CheckIfFullNameIsAPalindrome();
        }
    }
}