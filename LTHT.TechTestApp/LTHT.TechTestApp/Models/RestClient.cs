﻿namespace LTHT.TechTestApp.Models
{
    using System;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;

    using LTHT.TechTestApp.Interfaces.Models;

    using Newtonsoft.Json;

    public class RestClient : IRestClient
    {
        private readonly HttpClient httpClient;

        public RestClient()
        {
            this.httpClient = new HttpClient { BaseAddress = new Uri("http://ltht-tech-test.azurewebsites.net/api/") };
        }

        public async Task<T> DeleteAsync<T>(string requestUri)
        {
            var httpResponseMessage = await this.httpClient.DeleteAsync(requestUri).ConfigureAwait(false);

            return await GetResponseResult<T>(httpResponseMessage);
        }

        public async Task<T> GetAsync<T>(string requestUri)
        {
            var httpResponseMessage = await this.httpClient.GetAsync(requestUri).ConfigureAwait(false);
            return await GetResponseResult<T>(httpResponseMessage);
        }

        public async Task<T> PostAsync<TData, T>(string requestUri, TData data)
        {
            var jsonData = Serialize(data);
            var stringContent = GetStringContent(jsonData);
            var httpResponseMessage = await this.httpClient.PostAsync(requestUri, stringContent).ConfigureAwait(false);

            return await GetResponseResult<T>(httpResponseMessage);
        }

        public async Task<T> PutAsync<TData, T>(string requestUri, TData data)
        {
            var jsonData = Serialize(data);
            var stringContent = GetStringContent(jsonData);
            var response = await this.httpClient.PutAsync(requestUri, stringContent).ConfigureAwait(false);

            return await GetResponseResult<T>(response);
        }

        private static T Deserialize<T>(string json)
        {
            if (json == null)
            {
                return default(T);
            }

            try
            {
                return JsonConvert.DeserializeObject<T>(json);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private static async Task<T> GetResponseResult<T>(HttpResponseMessage httpResponseMessage)
        {
            httpResponseMessage.EnsureSuccessStatusCode();
            var jsonString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);

            return Deserialize<T>(jsonString);
        }

        private static StringContent GetStringContent(string json)
        {
            return new StringContent(json, Encoding.UTF8, "application/json");
        }

        private static string Serialize<T>(T data)
        {
            if (data == null)
            {
                return string.Empty;
            }

            return JsonConvert.SerializeObject(data);
        }
    }
}