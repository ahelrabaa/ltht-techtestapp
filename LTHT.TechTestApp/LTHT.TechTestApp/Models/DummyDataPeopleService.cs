namespace LTHT.TechTestApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using LTHT.TechTestApp.Interfaces.Models;

    public class DummyDataPeopleService : IPeopleService
    {
        public Task<Person> DeletePersonAsync(Person person)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Person>> GetPeopleAsync()
        {
            var people = new List<Person>();

            for (var c = 'a'; c <= 'z'; c++)
            {
                for (var i = 0; i < 3; i++)
                {
                    var p = new Person
                                {
                                    FirstName = $"{c}{i + 1}",
                                    LastName = $"{c}LastName",
                                    Colours =
                                        new List<Colour>
                                            {
                                                new Colour { Id = 1, Name = "Red" },
                                                new Colour { Id = 3, Name = "Blue" },
                                                new Colour { Id = 4, Name = "Pink" }
                                            }
                                };
                    people.Add(p);
                }
            }

            return Task.FromResult<IEnumerable<Person>>(people);
        }

        public Task<Person> SavePersonAsync(Person person)
        {
            throw new NotImplementedException();
        }
    }
}