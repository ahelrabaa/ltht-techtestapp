namespace LTHT.TechTestApp.Models
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using LTHT.TechTestApp.Interfaces.Models;

    public class DummyDataColoursService : IColoursService
    {
        public Task<IEnumerable<Colour>> GetColoursAsync()
        {
            var colours = new List<Colour>
                              {
                                  new Colour { Id = 1, Name = "Red" },
                                  new Colour { Id = 2, Name = "Green" },
                                  new Colour { Id = 3, Name = "Blue" },
                                  new Colour { Id = 4, Name = "Pink" },
                                  new Colour { Id = 5, Name = "Aqua" }
                              };

            return Task.FromResult<IEnumerable<Colour>>(colours);
        }
    }
}