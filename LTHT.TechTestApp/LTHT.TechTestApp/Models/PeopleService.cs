namespace LTHT.TechTestApp.Models
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using LTHT.TechTestApp.Interfaces.Models;

    public class PeopleService : IPeopleService
    {
        private readonly IRestClient restClient;

        public PeopleService(IRestClient restClient)
        {
            this.restClient = restClient;
        }

        public Task<Person> DeletePersonAsync(Person person)
        {
            return this.restClient.DeleteAsync<Person>($"people/{person.Id}");
        }

        public Task<IEnumerable<Person>> GetPeopleAsync()
        {
            return this.restClient.GetAsync<IEnumerable<Person>>("people?includeColours=true");
        }

        public Task<Person> SavePersonAsync(Person person)
        {
            if (person.Id > 0)
            {
                return this.restClient.PutAsync<Person, Person>($"people/{person.Id}", person);
            }

            return this.restClient.PostAsync<Person, Person>("people", person);
        }
    }
}