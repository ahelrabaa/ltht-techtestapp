namespace LTHT.TechTestApp.Models
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using LTHT.TechTestApp.Interfaces.Models;

    public class ColoursService : IColoursService
    {
        private readonly IRestClient restClient;

        public ColoursService(IRestClient restClient)
        {
            this.restClient = restClient;
        }

        public Task<IEnumerable<Colour>> GetColoursAsync()
        {
            return this.restClient.GetAsync<IEnumerable<Colour>>("colours");
        }
    }
}