namespace LTHT.TechTestApp.Models
{
    using System.Collections.Generic;

    using LTHT.TechTestApp.Helpers;

    public class Person : ObservableObject
    {
        private string firstName;

        private bool isAuthorised;

        private bool isEnabled;

        private string lastName;

        public IList<Colour> Colours { get; set; } = new List<Colour>();

        public string FirstName
        {
            get
            {
                return this.firstName;
            }

            set
            {
                this.firstName = value;
                this.OnPropertyChanged();
            }
        }

        public int Id { get; set; }

        public bool IsAuthorised
        {
            get
            {
                return this.isAuthorised;
            }

            set
            {
                this.isAuthorised = value;
                this.OnPropertyChanged();
            }
        }

        public bool IsEnabled
        {
            get
            {
                return this.isEnabled;
            }

            set
            {
                this.isEnabled = value;
                this.OnPropertyChanged();
            }
        }

        public bool IsValid { get; set; }

        public string LastName
        {
            get
            {
                return this.lastName;
            }

            set
            {
                this.lastName = value;
                this.OnPropertyChanged();
            }
        }
    }
}