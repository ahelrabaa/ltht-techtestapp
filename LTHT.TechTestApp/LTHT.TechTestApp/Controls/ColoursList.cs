﻿namespace LTHT.TechTestApp.Controls
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using LTHT.TechTestApp.Interfaces.ViewModels;
    using LTHT.TechTestApp.Models;

    using Xamarin.Forms;

    public partial class ColoursList : ContentView
    {
        public static readonly BindableProperty ColorsProperty = BindableProperty.Create(
            nameof(SelectedColours),
            typeof(IList<Colour>),
            typeof(ColoursList),
            default(IList<Colour>),
            propertyChanged: SelectedColoursPropertyChanged);

        private bool coloursLoaded;

        public ColoursList()
        {
            this.InitializeComponent();
        }

        public IList<Colour> SelectedColours
        {
            get
            {
                return (IList<Colour>)this.GetValue(ColorsProperty);
            }

            set
            {
                this.SetValue(ColorsProperty, value);
            }
        }

        public IColoursViewModel ViewModel { get; private set; }

        protected override async void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
            this.ViewModel = this.BindingContext as IColoursViewModel;
            await this.LoadColoursAsync();
        }

        private static void SelectedColoursPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var colorsList = (ColoursList)bindable;
            colorsList.LoadColors((IEnumerable<IColourViewModel>)newValue);
        }

        private void LoadColors(IEnumerable<IColourViewModel> colourViewModels)
        {
            if (colourViewModels == null)
            {
                return;
            }

            var colourSwitchTemplate = (DataTemplate)this.Resources["ColourSwitchTemplate"];
            foreach (var colourViewModel in colourViewModels)
            {
                var headeredSwitch = (HeaderedSwitch)colourSwitchTemplate.CreateContent();
                headeredSwitch.BindingContext = colourViewModel;
                this.MainList.Children.Add(headeredSwitch);
            }
        }

        private async Task LoadColoursAsync()
        {
            if (this.coloursLoaded)
            {
                return;
            }

            var coloursViewModel = this.ViewModel;
            if (coloursViewModel != null)
            {
                this.coloursLoaded = true;

                await coloursViewModel.Initialization;
                this.LoadColors(coloursViewModel.Colours);
            }
        }
    }
}