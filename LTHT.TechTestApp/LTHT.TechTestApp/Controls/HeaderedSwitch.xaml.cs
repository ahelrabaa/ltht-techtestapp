﻿namespace LTHT.TechTestApp.Controls
{
    using System;

    using Xamarin.Forms;

    public partial class HeaderedSwitch : ContentView
    {
        public static readonly BindableProperty HeaderProperty = BindableProperty.Create(
            nameof(Header),
            typeof(string),
            typeof(HeaderedSwitch),
            propertyChanged: HeaderPropertyChanged);

        public static readonly BindableProperty IsToggledProperty = BindableProperty.Create(
            nameof(IsToggled),
            typeof(bool),
            typeof(HeaderedSwitch),
            default(bool),
            BindingMode.TwoWay,
            propertyChanged: IsToggledPropertyChanged);

        public HeaderedSwitch()
        {
            this.InitializeComponent();
        }

        public string Header
        {
            get
            {
                return (string)this.GetValue(HeaderProperty);
            }

            set
            {
                this.SetValue(HeaderProperty, value);
            }
        }

        public bool IsToggled
        {
            get
            {
                return (bool)this.GetValue(IsToggledProperty);
            }

            set
            {
                this.SetValue(IsToggledProperty, value);
            }
        }

        public bool ToggleHeaderVisibility { get; set; }

        private static void HeaderPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var headeredSwitch = (HeaderedSwitch)bindable;
            headeredSwitch.HeaderLabel.Text = newValue as string;
        }

        private static void IsToggledPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var headeredSwitch = (HeaderedSwitch)bindable;
            headeredSwitch.MainSwitch.IsToggled = (bool)newValue;
        }

        private void MainSwitchOnToggled(object sender, ToggledEventArgs e)
        {
            this.IsToggled = e.Value;
        }

        private async void TapGestureRecognizerOnTapped(object sender, EventArgs e)
        {
            if (!this.ToggleHeaderVisibility)
            {
                return;
            }

            if (this.MainSwitch.Opacity < 1)
            {
                await this.MainSwitch.FadeTo(1);
                this.MainSwitch.IsEnabled = true;
            }
            else
            {
                await this.MainSwitch.FadeTo(0);
                this.MainSwitch.IsEnabled = false;
            }
        }
    }
}