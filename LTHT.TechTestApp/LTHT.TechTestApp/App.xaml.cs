﻿namespace LTHT.TechTestApp
{
    using LTHT.TechTestApp.ViewModels;
    using LTHT.TechTestApp.Views;

    using Xamarin.Forms;

    public partial class App : Application
    {
        public App()
        {
            this.InitializeComponent();
            ViewModelLocator = new ViewModelLocator();
            this.MainPage = new NavigationPage(new MainPage());
        }

        public static ViewModelLocator ViewModelLocator { get; private set; }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }
    }
}