﻿namespace LTHT.TechTestApp.Interfaces.ViewModels
{
    using System.Threading.Tasks;
    using System.Windows.Input;

    public interface IPersonDetailPageViewModel : IViewModel
    {
        IColoursViewModel ColoursViewModel { get; }

        ICommand DeleteCommand { get; }

        ICommand GoBackCommand { get; }

        Task Initialization { get; }

        bool IsDeleteEnabled { get; }

        IPersonViewModel PersonViewModel { get; }

        ICommand SaveCommand { get; }

        string Title { get; }
    }
}