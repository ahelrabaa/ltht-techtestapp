﻿namespace LTHT.TechTestApp.Interfaces.ViewModels
{
    using System.ComponentModel;

    public interface IViewModel : INotifyPropertyChanged
    {
        bool IsBusy { get; }
    }
}