﻿namespace LTHT.TechTestApp.Interfaces.ViewModels
{
    using System.Collections.Generic;

    using LTHT.TechTestApp.Models;

    public interface IPersonViewModel : IViewModel
    {
        string FirstName { get; }

        string FirstNameInitial { get; }

        string FullName { get; }

        bool HasPalindromeFullName { get; }

        string LastName { get; }

        Person Person { get; }

        IList<IColourViewModel> SelectedColours { get; }

        void UpdateSelectedColours(IList<IColourViewModel> selectedColours);
    }
}