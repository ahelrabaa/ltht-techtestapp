﻿namespace LTHT.TechTestApp.Interfaces.ViewModels
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IColoursViewModel : IViewModel
    {
        IList<IColourViewModel> Colours { get; }

        Task Initialization { get; }

        IList<IColourViewModel> SelectedColours { get; }
    }
}