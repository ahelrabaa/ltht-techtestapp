namespace LTHT.TechTestApp.Interfaces.ViewModels
{
    using LTHT.TechTestApp.Models;

    public interface IColourViewModel : IViewModel
    {
        Colour Colour { get; }

        bool IsSelected { get; set; }
    }
}