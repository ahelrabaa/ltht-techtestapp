﻿namespace LTHT.TechTestApp.Interfaces.ViewModels
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Windows.Input;

    using LTHT.TechTestApp.Helpers;

    public interface IMainPageViewModel : IViewModel
    {
        ICommand FilterPeopleCommand { get; }

        Task Initialization { get; }

        bool IsRefreshing { get; }

        IList<Grouping<string, IPersonViewModel>> People { get; }

        ICommand RefreshPeopleCommand { get; }

        ICommand ViewPersonDetailCommand { get; }
    }
}