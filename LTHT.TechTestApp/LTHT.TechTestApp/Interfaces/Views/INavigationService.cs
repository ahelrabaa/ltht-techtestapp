﻿namespace LTHT.TechTestApp.Interfaces.Views
{
    using System.Threading.Tasks;

    using LTHT.TechTestApp.Interfaces.ViewModels;

    public interface INavigationService
    {
        Task GoBackAsync();

        Task NavigateToPersonDetialAsync(IPersonViewModel personViewModel);
    }
}