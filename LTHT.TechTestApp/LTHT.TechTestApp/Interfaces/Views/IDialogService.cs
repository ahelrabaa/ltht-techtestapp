﻿namespace LTHT.TechTestApp.Interfaces.Views
{
    using System.Threading.Tasks;

    public interface IDialogService
    {
        Task DisplayMessageAsync(string title, string message);
    }
}