namespace LTHT.TechTestApp.Interfaces.Models
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using LTHT.TechTestApp.Models;

    public interface IPeopleService
    {
        Task<IEnumerable<Person>> GetPeopleAsync();
        Task<Person> DeletePersonAsync(Person person);
        Task<Person> SavePersonAsync(Person person);


    }
}