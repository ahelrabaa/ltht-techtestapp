﻿namespace LTHT.TechTestApp.Interfaces.Models
{
    using System.Threading.Tasks;

    public interface IRestClient
    {
        Task<T> DeleteAsync<T>(string requestUri);

        Task<T> GetAsync<T>(string requestUri);

        Task<T> PostAsync<TData, T>(string requestUri, TData data);

        Task<T> PutAsync<TData, T>(string requestUri, TData data);
    }
}