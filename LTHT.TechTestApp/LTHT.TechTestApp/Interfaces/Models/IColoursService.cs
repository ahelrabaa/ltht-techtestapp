namespace LTHT.TechTestApp.Interfaces.Models
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using LTHT.TechTestApp.Models;

    public interface IColoursService
    {
        Task<IEnumerable<Colour>> GetColoursAsync();
    }
}