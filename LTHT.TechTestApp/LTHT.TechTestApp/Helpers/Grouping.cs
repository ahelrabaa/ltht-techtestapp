namespace LTHT.TechTestApp.Helpers
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    public class Grouping<K, T> : ObservableCollection<T>
    {
        public Grouping(K key, IEnumerable<T> items)
        {
            this.Key = key;

            foreach (var item in items)
            {
                this.Items.Add(item);
            }
        }

        public K Key { get; private set; }
    }
}