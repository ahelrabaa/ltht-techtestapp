﻿namespace LTHT.TechTestApp.Views
{
    using System.Threading.Tasks;

    using LTHT.TechTestApp.Interfaces.Views;

    using Xamarin.Forms;

    public class DialogService : IDialogService
    {
        public Task DisplayMessageAsync(string title, string message)
        {
            return Application.Current.MainPage.DisplayAlert(title, message, "ok");
        }
    }
}