﻿namespace LTHT.TechTestApp.Views
{
    using LTHT.TechTestApp.Interfaces.ViewModels;

    using Xamarin.Forms;

    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            this.ViewModel = App.ViewModelLocator.MainPageViewModel;
            this.InitializeComponent();
        }

        public IMainPageViewModel ViewModel { get; }

        public void ListViewOnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return;
            }

            this.ViewModel.ViewPersonDetailCommand.Execute(e.SelectedItem);
            this.PeopleListView.SelectedItem = null;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await this.ViewModel.Initialization;
        }

        private void EntryOnTextChanged(object sender, TextChangedEventArgs e)
        {
            this.ViewModel.FilterPeopleCommand.Execute(e.NewTextValue);
        }
    }
}