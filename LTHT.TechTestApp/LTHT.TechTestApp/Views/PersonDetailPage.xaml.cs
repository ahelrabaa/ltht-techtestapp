﻿namespace LTHT.TechTestApp.Views
{
    using LTHT.TechTestApp.Interfaces.ViewModels;

    using Xamarin.Forms;

    public partial class PersonDetailPage : ContentPage
    {
        public PersonDetailPage(IPersonViewModel personViewModel)
        {
            this.ViewModel = App.ViewModelLocator.PersonDetailPageViewModel(personViewModel);
            this.InitializeComponent();
        }

        public IPersonDetailPageViewModel ViewModel { get; }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await this.ViewModel.Initialization;
        }
    }
}