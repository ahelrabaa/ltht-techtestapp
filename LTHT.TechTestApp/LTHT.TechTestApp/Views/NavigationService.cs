namespace LTHT.TechTestApp.Views
{
    using System.Threading.Tasks;

    using LTHT.TechTestApp.Interfaces.ViewModels;
    using LTHT.TechTestApp.Interfaces.Views;

    using Xamarin.Forms;

    public class NavigationService : INavigationService
    {
        private static INavigation Navigation => Application.Current.MainPage.Navigation;

        public Task GoBackAsync()
        {
            return Navigation.PopModalAsync();
        }

        public Task NavigateToPersonDetialAsync(IPersonViewModel personViewModel)
        {
            return Navigation.PushModalAsync(new NavigationPage(new PersonDetailPage(personViewModel)));
        }
    }
}